Path1080ti='/mnt/Shared_Data/BottleBrush/surfacepolymers/2D/dispersex10/';
% StartingPointPath='/mnt/Shared_Data/BottleBrush/6_16_20/';

SinglePath=1; %1 means don't change directories based on pbs, use Path1080ti  2DOESN'TWORK%2 means search all directories using "Starting point path" Need to set 180ti path too
Just1080ti=0;
JustTitan=0;
GPUall4=1;
GPUall3=0;
GPUall5=0;
if Just1080ti==1 && JustTitan ==1;
    error('can''t be just 1080ti and just titan')
end
if (GPUall4==1 || GPUall3==1 || GPUall5==1) && SinglePath~=1
    error('GPU4 and GPU3 only work with single path for now')
end
MultiPathPrefix='/mnt/Shared_Data/PlasticRoads/12-11-20_cohe/';

SearchPath='/mnt/Shared_Data/PlasticRoads/12-15-20_cohe/';
WritePath='/mnt/Shared_Data/PlasticRoads/12-15-20_cohe/';

MultiPathPrefix=Path1080ti;
SearchPath=Path1080ti;
WritePath=Path1080ti;


% make titan path
if ~strcmp(Path1080ti(1:17),'/mnt/Shared_Data/')
    error('starting path doesn'' begin with ''/mnt/Shared_Data/''');
end

PathTitan=['/mnt/RAID1/Insync/Shared_Data/' Path1080ti(18:end)];



%findwriting counter
fileswrite=dir(WritePath);
PyArray=[];
for i=3:length(fileswrite)
    if strcmp(fileswrite(i).name(1:min(9,length(fileswrite(i).name))),'run1080ti')
        file=strtok(fileswrite(i).name,'.');
        PyArray(end+1)=str2num(file(10:end));
    elseif strcmp(fileswrite(i).name(1:min(8,length(fileswrite(i).name))),'runtitan')
        file=strtok(fileswrite(i).name,'.');
        PyArray(end+1)=str2num(file(9:end));
    end
end

Counter=max(PyArray)+1;
        


%startwritingshfiles
if Just1080ti==1
    fid1080ti=fopen(['run1080ti' num2str(Counter) '.sh'],'w','l');
    if SinglePath==1
        fprintf(fid1080ti,['cd ' Path1080ti '\n']);
    end
elseif JustTitan==1
    fidTitan=fopen(['runtitan' num2str(Counter) '.sh'],'w','l');
    if SinglePath==1
        fprintf(fidTitan,['cd ' PathTitan '\n']);
    end
elseif GPUall3==0 && GPUall4==0 && GPUall5==0
    fidTitan=fopen(['runtitan' num2str(Counter) '.sh'],'w','l');
    fid1080ti=fopen(['run1080ti' num2str(Counter) '.sh'],'w','l');
    if SinglePath==1
        fprintf(fidTitan,['cd ' PathTitan '\n']);
        fprintf(fid1080ti,['cd ' Path1080ti '\n']);
    end
elseif GPUall3==1
    fid1=fopen(['run1' num2str(Counter) '.sh'],'w','l');
    fid2=fopen(['run2' num2str(Counter) '.sh'],'w','l');
    fid3=fopen(['run3' num2str(Counter) '.sh'],'w','l');
    fprintf(fid1,['cd ' PathTitan '\n']);
    fprintf(fid2,['cd ' PathTitan '\n']);
    fprintf(fid3,['cd ' PathTitan '\n']);
elseif GPUall4==1
    fid1=fopen(['run1' num2str(Counter) '.sh'],'w','l');
    fid2=fopen(['run2' num2str(Counter) '.sh'],'w','l');
    fid3=fopen(['run3' num2str(Counter) '.sh'],'w','l');
    fid0=fopen(['run0' num2str(Counter) '.sh'],'w','l');
    fprintf(fid1,['cd ' PathTitan '\n']);
    fprintf(fid2,['cd ' PathTitan '\n']);
    fprintf(fid3,['cd ' PathTitan '\n']);
    fprintf(fid0,['cd ' PathTitan '\n']);
elseif GPUall5==1
    fid1=fopen(['run1' num2str(Counter) '.sh'],'w','l');
    fid2=fopen(['run2' num2str(Counter) '.sh'],'w','l');
    fid3=fopen(['run3' num2str(Counter) '.sh'],'w','l');
    fid0=fopen(['run0' num2str(Counter) '.sh'],'w','l');
    fid4=fopen(['run0_office' num2str(Counter) '.sh'],'w','l'); %4=office
    fprintf(fid1,['cd ' PathTitan '\n']);
    fprintf(fid2,['cd ' PathTitan '\n']);
    fprintf(fid3,['cd ' PathTitan '\n']);
    fprintf(fid0,['cd ' PathTitan '\n']);
    fprintf(fid4,['cd ' Path1080ti '\n']); %4=office
end


files=dir(SearchPath);
counter=0;
if GPUall4==0 && GPUall3==0 && GPUall5==0
    for i=3:length(files)
        if endsWith(files(i).name,'.py')
            counter=counter+1;
            %% account for only a single run
            if JustTitan==1
                if mod(counter,2)~=1
                    counter=counter+1;
                end
            end
            if Just1080ti==1
                if mod(counter,2)==1
                    counter=counter+1;
                end
            end
            %%
            if SinglePath~=1
                fid1=fopen([SearchPath files(i).name(1:end-2) 'pbs'],'r');
                testread=0;
                while testread==0
                    x=fgetl(fid1);
                    if ~isempty(strfind(x,'cd /work/apeters/'));
                        testread=1;
                    end
                end
                path=[MultiPathPrefix x(18:end-1)];
            end      

            if mod(counter,2)==1
                if SinglePath~=1
                    path2=['/mnt/RAID1/Insync/Shared_Data/' path(18:end)];
                    fprintf(fidTitan,['cd ' path2 '\n'])
                end
                fprintf(fidTitan,['python ' files(i).name ' --mode=gpu --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            else
                if SinglePath~=1
                    fprintf(fid1080ti,['cd ' path '\n'])
                end
                fprintf(fid1080ti,['python ' files(i).name ' --mode=gpu --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            end
        end
    end
elseif GPUall3==1
    for i=3:length(files)
        if strcmp(files(i).name(end-2:end),'.py')
            counter=counter+1;
            if rem(counter,3)==0
                fprintf(fid1,['python ' files(i).name ' --gpu=1 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,3)==1
                fprintf(fid2,['python ' files(i).name ' --gpu=2 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,3)==2
                fprintf(fid3,['python ' files(i).name ' --gpu=3 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            end
        end
    end
elseif GPUall4==1
    for i=3:length(files)
        if strcmp(files(i).name(end-2:end),'.py')
            counter=counter+1;
            if rem(counter,4)==0
                fprintf(fid1,['python ' files(i).name ' --gpu=1 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,4)==1
                fprintf(fid2,['python ' files(i).name ' --gpu=2 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,4)==2
                fprintf(fid3,['python ' files(i).name ' --gpu=3 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,4)==3
                fprintf(fid0,['python ' files(i).name ' --gpu=0 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            end
        end
    end
elseif GPUall5==1
    for i=3:length(files)
        if strcmp(files(i).name(end-2:end),'.py')
            counter=counter+1;
            if rem(counter,5)==0
                fprintf(fid1,['python ' files(i).name ' --gpu=1 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,5)==1
                fprintf(fid2,['python ' files(i).name ' --gpu=2 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,5)==2
                fprintf(fid3,['python ' files(i).name ' --gpu=3 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,5)==3
                fprintf(fid0,['python ' files(i).name ' --gpu=0 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            elseif rem(counter,5)==4
                fprintf(fid4,['python ' files(i).name ' --gpu=0 --msg-file=''' files(i).name(1:end-2) 'o''\n']);
            end
        end
    end
end
    

