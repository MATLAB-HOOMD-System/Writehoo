clear all
close all
fclose all

StartingPath='/mnt/Shared_Data/BottleBrush/6_22_20_Server_dihedrals_1/';
GPUnames=[2 3];
numGPUS=length(GPUnames);
numCPUS=0;
fracloadcpu=0.25;

%%%%%%%%%%%%%%%%%%
% if numGPUS==4
%     startingGPU=0;
% elseif numGPUS==3
%     startingGPU=1;
% else
%     error('unrecognized number of GPUS')
% end
for i=GPUnames
    eval(['fid' num2str(i) '=fopen(''run' num2str(i) '.sh'',''w'');']);
end

for i=1:numCPUS
    eval(['fidcpu' num2str(i) '=fopen(''runcpu' num2str(i) '.sh'',''w'');']);
end


GPUloopsPerCPUloops=round(1/fracloadcpu)
totalloopsize=numGPUS*GPUloopsPerCPUloops+numCPUS

files=dir([StartingPath '**/*.py']);

%To put both gpu and cpu together
totalcounter=-1;
for i=1:size(files,1)
    totalcounter=totalcounter+1;
    totalcounter
    totalloopsize
    gpulimit=numGPUS*GPUloopsPerCPUloops
    gpuorcpu=rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
    if rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
        gpunum=rem(rem(totalcounter,totalloopsize),numGPUS)
%         if gpunum==0 && numGPUS==3
%             gpunum=3;
%         end
    gpunumname=GPUnames(gpunum+1);
        eval(['fid=fid' num2str(gpunumname) ';']);
        fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --gpu=' num2str(gpunumname) ' --msg-file=''' files(i).name(1:end-2) 'o''\n']);
    else %cpus
        cpunum=rem(totalcounter,totalloopsize)-numGPUS*GPUloopsPerCPUloops+1
        eval(['fid=fidcpu' num2str(cpunum) ';']);
        fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --mode=cpu --msg-file=''' files(i).name(1:end-2) 'o''\n']);
    end
end




% 
% %enable to filter CPU and GPU
% filescounter2=0;
% filescounter3=0;
% for i=1:size(files,1)
%     files(i);
%     x=split(files(i).name,'_');
%     if length(x)<6
%         x{6}='0';
%     end
%     x1=regexp(x{3},'\d*','Match');
%     x1=x1{1};
%     x2=regexp(x{4},'\d*','Match');
%     x2=x2{1};
%     x3=regexp(x{6},'\d*','Match');
%     x3=x3{1};
%     if ((str2num(x2)>12 || str2num(x3)>12) || (str2num(x1)==60 && (str2num(x2)>10 || str2num(x3)>12)))
%         filescounter2=filescounter2+1;
%         files2(filescounter2)=files(i);
%     else
%         filescounter3=filescounter3+1;
%         files3(filescounter3)=files(i);
%     end
% end
% %files2 is big (GPU)
% %files3 is small (CPU)
% size(files2)
% size(files3)
% filesall=files;
% 
% %Just GPU
% files=files2;
% numCPUStemp=0;
% GPUloopsPerCPUloops=round(1/fracloadcpu)
% totalloopsize=numGPUS*GPUloopsPerCPUloops+numCPUStemp
% totalcounter=-1;
% for i=1:size(files,2)
%     totalcounter=totalcounter+1;
%     totalcounter
%     totalloopsize
%     gpulimit=numGPUS*GPUloopsPerCPUloops
%     gpuorcpu=rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
%     if rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
%         gpunum=rem(rem(totalcounter,totalloopsize),numGPUS)
% %         if gpunum==0 && numGPUS==3
% %             gpunum=3;
% %         end
%     gpunumname=GPUnames(gpunum+1);
%         eval(['fid=fid' num2str(gpunumname) ';']);
%         fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --gpu=' num2str(gpunumname) ' --msg-file=''' files(i).name(1:end-2) 'o''\n']);
%     else %cpus
%         cpunum=rem(totalcounter,totalloopsize)-numGPUS*GPUloopsPerCPUloops+1
%         eval(['fid=fidcpu' num2str(cpunum) ';']);
%         fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --mode=cpu --msg-file=''' files(i).name(1:end-2) 'o''\n']);
%     end
% end
% 
% %Just CPU
% files=files3;
% numGPUS=0;
% numCPUStemp=numCPUS;
% GPUloopsPerCPUloops=round(1/fracloadcpu)
% totalloopsize=numGPUS*GPUloopsPerCPUloops+numCPUStemp
% totalcounter=-1;
% for i=1:size(files,2)
%     totalcounter=totalcounter+1;
%     totalcounter
%     totalloopsize
%     gpulimit=numGPUS*GPUloopsPerCPUloops
%     gpuorcpu=rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
%     if rem(totalcounter,totalloopsize)<numGPUS*GPUloopsPerCPUloops
%         gpunum=rem(rem(totalcounter,totalloopsize),numGPUS)
% %         if gpunum==0 && numGPUS==3
% %             gpunum=3;
% %         end
%     gpunumname=GPUnames(gpunum+1);
%         eval(['fid=fid' num2str(gpunumname) ';']);
%         fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --gpu=' num2str(gpunumname) ' --msg-file=''' files(i).name(1:end-2) 'o''\n']);
%     else %cpus
%         cpunum=rem(totalcounter,totalloopsize)-numGPUS*GPUloopsPerCPUloops+1
%         eval(['fid=fidcpu' num2str(cpunum) ';']);
%         fprintf(fid,['cd ' files(i).folder '/ ; python ' files(i).name ' --mode=cpu --msg-file=''' files(i).name(1:end-2) 'o''\n']);
%     end
% end
