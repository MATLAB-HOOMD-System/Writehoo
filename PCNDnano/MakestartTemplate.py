#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
from hoomd import cgcmm
import sys
import numpy
import calendar
import time
import math
from random import *

c = context.initialize()

prefix='Make_PCNDNano110_r2_N18NA9_nG60'
Xi=0.1
Tau=100
alphaAB=25
totaltimesteps=10000
timesteps_afterPCND=100000
numnano=110;########
numpoly=6600;##########
finalboxdim=35.1115########
initboxdim=finalboxdim*20
partradius=2#########
dmax=partradius*2
partmass=7.98*(4/3)*3.14159*partradius**3

if os.path.isfile(prefix+'_traj.dcd'):
	os.remove(prefix+'_traj.dcd');
if os.path.isfile(prefix+'_traj2.dcd'):
	os.remove(prefix+'_traj2.dcd');

system=deprecated.init.read_xml(filename="PCNDNano110_r2_N18NA9_nG60.xml")
#init.read_gsd(filename='PCNDnanotest_mixed.gsd')
#gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
restart_flag=0

#########Set up forcefields and integrators
nl1 = md.nlist.tree(r_buff=2)
nl2 = md.nlist.tree(r_buff=2)
c.sorter.set_params(grid=2)
c.sorter.disable()
#option.set_autotuner_params(period=1)
#######DPD
dpd = md.pair.dpd(r_cut=1.0, nlist=nl2, kT=1.0, seed=calendar.timegm(time.gmtime())) 
dpd.pair_coeff.set('A', 'A', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 5,r_cut=-1)


dpd.pair_coeff.set('B', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 5,r_cut=-1)

dpd.pair_coeff.set('C', 'C', A=25, gamma = 5)
dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 5)

dpd.pair_coeff.set('D', 'D', A=25, gamma = 5)

##############WCADPD
sigmaWCA=(dmax+1)/2
gammaWCA=5*(((partmass+1)/2)**(1/2))
gammaWCA=5
gammaWCAPP=0.05
WCA = md.pair.dpdlj(r_cut=sigmaWCA*2**(1.0/6), nlist=nl1,kT=1.0, seed=calendar.timegm(time.gmtime())+1)
WCA.pair_coeff.set('A', 'A', epsilon=0.0, sigma=dmax, gamma = gammaWCAPP, r_on = 100)
WCA.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0, gamma = gammaWCA,r_cut=-1)
WCA.pair_coeff.set('A', 'C', epsilon=0.0, sigma=sigmaWCA, gamma = gammaWCA ,r_on = 100)
WCA.pair_coeff.set('A', 'D', epsilon=0.0, sigma=sigmaWCA, gamma = gammaWCA ,r_on = 100)

WCA.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)

WCA.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)

WCA.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
#########################

##############WCA
WCA = md.pair.slj(r_cut=2**(1.0/6), nlist=nl1,d_max = dmax)
WCA.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0,r_on = 100)
WCA.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA.pair_coeff.set('A', 'C', epsilon=1.0, sigma=1.0,r_on = 100)
WCA.pair_coeff.set('A', 'D', epsilon=1.0, sigma=1.0,r_on = 100)

WCA.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)

WCA.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)

WCA.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)
#########################


harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)

groupA = group.type(name='a-particles', type='A')
groupB = group.type(name='b-particles', type='B')
groupC = group.type(name='c-particles', type='C')
groupD = group.type(name='d-particles', type='D')


groupAB = group.union(name="ab-particles", a=groupA, b=groupB)  #diblocks
groupCD = group.union(name="cd-particles", a=groupC, b=groupD)  #triblocks
groupACD = group.union(name="acd-particles", a=groupA, b=groupCD) #polymers and centers
groupBCD = group.union(name="bcd-particles", a=groupB, b=groupCD) #polymers and centers
groupABCD = group.union(name="abcd-particles", a=groupAB, b=groupCD) #all

###############Rigid Body
rigid = md.constrain.rigid();
rigid.set_param('A',types=['B']*62,positions=[ (0.44504,0,1.9499),(-0.22252,0.38542,1.9499),(-0.22252,-0.38542,1.9499),(1.247,0,1.5637),(0.95524,0.80154,1.5637),(0.21654,1.228,1.5637),(-0.62349,1.0799,1.5637),(-1.1718,0.42649,1.5637),(-1.1718,-0.42649,1.5637),(-0.62349,-1.0799,1.5637),(0.21654,-1.228,1.5637),(0.95524,-0.80154,1.5637),(1.8019,0,0.86777),(1.5605,0.90097,0.86777),(0.90097,1.5605,0.86777),(1.1034e-16,1.8019,0.86777),(-0.90097,1.5605,0.86777),(-1.5605,0.90097,0.86777),(-1.8019,2.2067e-16,0.86777),(-1.5605,-0.90097,0.86777),(-0.90097,-1.5605,0.86777),(-3.3101e-16,-1.8019,0.86777),(0.90097,-1.5605,0.86777),(1.5605,-0.90097,0.86777),(2,0,1.2246e-16),(1.8019,0.86777,1.2246e-16),(1.247,1.5637,1.2246e-16),(0.44504,1.9499,1.2246e-16),(-0.44504,1.9499,1.2246e-16),(-1.247,1.5637,1.2246e-16),(-1.8019,0.86777,1.2246e-16),(-2,2.4493e-16,1.2246e-16),(-1.8019,-0.86777,1.2246e-16),(-1.247,-1.5637,1.2246e-16),(-0.44504,-1.9499,1.2246e-16),(0.44504,-1.9499,1.2246e-16),(1.247,-1.5637,1.2246e-16),(1.8019,-0.86777,1.2246e-16),(1.8019,0,-0.86777),(1.5605,0.90097,-0.86777),(0.90097,1.5605,-0.86777),(1.1034e-16,1.8019,-0.86777),(-0.90097,1.5605,-0.86777),(-1.5605,0.90097,-0.86777),(-1.8019,2.2067e-16,-0.86777),(-1.5605,-0.90097,-0.86777),(-0.90097,-1.5605,-0.86777),(-3.3101e-16,-1.8019,-0.86777),(0.90097,-1.5605,-0.86777),(1.5605,-0.90097,-0.86777),(1.247,0,-1.5637),(0.95524,0.80154,-1.5637),(0.21654,1.228,-1.5637),(-0.62349,1.0799,-1.5637),(-1.1718,0.42649,-1.5637),(-1.1718,-0.42649,-1.5637),(-0.62349,-1.0799,-1.5637),(0.21654,-1.228,-1.5637),(0.95524,-0.80154,-1.5637),(0.44504,0,-1.9499),(-0.22252,0.38542,-1.9499),(-0.22252,-0.38542,-1.9499)]) #for r=2



rigid.validate_bodies()


integrate=md.integrate.mode_standard(dt=0.001)
nve=md.integrate.nve(group=groupACD)
zeroer= md.update.zero_momentum(period=1)
DCD1=dump.dcd(filename=prefix+"_traj.dcd", group=groupABCD, period=100, phase=0)

run(1000,profile=True)
zeroer.disable()
run(1000)

boxsize=update.box_resize(L = variant.linear_interp([(0, initboxdim), (5000, finalboxdim*3)]))
run(6000)
boxsize=update.box_resize(L = variant.linear_interp([(0, finalboxdim*3), (10000, finalboxdim*1.1)]))
run(11000)
boxsize=update.box_resize(L = variant.linear_interp([(0, finalboxdim*1.1), (5000, finalboxdim*1)]))
run(6000)


'''
for i in range(199):
	print(i)
	boxsize=update.box_resize(L = variant.linear_interp([(0, finalboxdim*(20-i/10)), (100, finalboxdim*(20-i/10-0.1))]))
	run(100)
	nve.disable()
	del nve
	del integrate
	fire=md.integrate.mode_minimize_fire(dt=0.01)
	nve=md.integrate.nve(group=groupACD)
	run(100)
	nve.disable()
	del nve
	del fire
	integrate=md.integrate.mode_standard(dt=0.02)
	nve=md.integrate.nve(group=groupACD)
'''
nl1.set_params(r_buff = 1.5, check_period = 1)
nl2.set_params(r_buff = 1.5, check_period = 1)
'''
nve.disable()
del nve
del integrate


fire=md.integrate.mode_minimize_fire(dt=0.001)
nve=md.integrate.nve(group=groupACD)
run(3000)

nve.disable()
del nve
del fire
'''

#integrate=md.integrate.mode_standard(dt=0.001)
#nve=md.integrate.nve(group=groupACD)
zeroer.enable()
run(100)
zeroer.disable()

for i in range(1,11):
	print("\n\n",i,"\n\n")
	integrate.set_params(dt=i/1000)
	run(500,profile=True)
integrate.set_params(dt=0.01)
run(5000,profile=True)
#run(10000) #mixup at beginning
xmlstart = deprecated.dump.xml(group=group.all(), filename=prefix+"_min.xml", position=True,mass=True,diameter=True,type=True,body=True,bond=True,angle=True,dihedral=True,constraint=True,improper=True,charge=True,orientation=True,angmom=False,inertia=True)
#DCD1=dump.dcd(filename=prefix+"_traj.dcd", group=groupABCD, period=100, phase=0)
#gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
#analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_harmonic_energy','pressure' ],period=100, header_prefix='#', phase=0)
analyze.log(filename=prefix+'.log', quantities=['time','angle_cgcmm_energy' ],period=100, header_prefix='#', phase=0)

PCND = cgcmm.angle.cgcmm()
for i in range(numnano):
	PCND.set_coeff('Nanoparticle' + str(i+1), k = Xi, t0 =Tau, exponents ='individual',epsilon=numnano+numpoly,sigma=i) 
for i in range(numpoly):
	print(i)
	PCND.set_coeff('Polymer' + str(i+1), k = Xi, t0 =Tau, exponents ='individual',epsilon=numnano+numpoly,sigma=i+numnano) 
alphaAB=100
###################################################
dpd.pair_coeff.set('A', 'A', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 5,r_cut=-1)


dpd.pair_coeff.set('B', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 5,r_cut=-1)

dpd.pair_coeff.set('C', 'C', A=25, gamma = 5)
dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 5)

dpd.pair_coeff.set('D', 'D', A=25, gamma = 5)
#####################################################
zeroer.enable()
run(10000,profile=True)
xmlstart = deprecated.dump.xml(group=group.all(), filename=prefix+"_start.xml", position=True,mass=True,diameter=True,type=True,body=True,bond=True,angle=True,dihedral=True,constraint=True,improper=True,charge=True,orientation=True,angmom=False,inertia=True)
