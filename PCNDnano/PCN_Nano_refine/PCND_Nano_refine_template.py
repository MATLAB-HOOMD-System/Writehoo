
#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
from hoomd import cgcmm
import sys
import numpy
import calendar
import time
import math
from random import *
c = context.initialize()

prefix='PCND_Nano_refine_template'
XiNano=0.02
XiPoly=0.02
TauNano=100000
tauPoly=100000
alphaAB=100
totaltimesteps=20000000
timesteps_afterPCND=1000000
numnano= 110;
numpoly= 6600;
mix_time_steps= 10000;
dcd_period= 10000;
partradius= 2;
dmax=partradius*2
partmass=7.98*(4/3)*3.14159*partradius**3

if os.path.isfile(prefix+'_AfterPCND1final.xml'):
	exit()
elif os.path.isfile(prefix+'_AfterPCND1restart.gsd'):
	init.read_gsd(filename=prefix+'_AfterPCND1restart.gsd')
	restart_flag=3
elif os.path.isfile(prefix+'_PCND1final.xml'):
	system=deprecated.init.read_xml(filename=prefix+'_PCND1final.xml')
	restart_flag=2
elif os.path.isfile(prefix+'_PCND1restart.gsd'):
	init.read_gsd(filename=prefix+'_PCND1restart.gsd')
	restart_flag=1
else:
	system=deprecated.init.read_xml(filename="Make_PCNDNano110_r2_N18NA9_nG60_start.xml",time_step=0)
	restart_flag=0

#########Set up forcefields and integrators
nlDPD = md.nlist.cell(r_buff=0.5);
nlNANO = md.nlist.tree(r_buff=1.9);
c.sorter.set_params(grid=2)
c.sorter.disable()
#######DPD
dpd = md.pair.dpd(r_cut=1.0, nlist=nlDPD, kT=1.0, seed=calendar.timegm(time.gmtime())) 
dpd.pair_coeff.set('A', 'A', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'B', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 5,r_cut=-1)
dpd.pair_coeff.set('C', 'C', A=25, gamma = 5)
dpd.pair_coeff.set('C', 'D', A=25, gamma = 5)
dpd.pair_coeff.set('D', 'D', A=25, gamma = 5)
##############WCADPD
sigmaWCA=(dmax+1)/2
gammaWCA=5*(((partmass+1)/2)**(1/2))
gammaWCA=5
gammaWCAPP=0.05
WCA = md.pair.dpdlj(r_cut=sigmaWCA*2**(1.0/6), nlist=nlNANO,kT=1.0, seed=calendar.timegm(time.gmtime())+1)
WCA.pair_coeff.set('A', 'A', epsilon=0.0, sigma=dmax, gamma = gammaWCAPP, r_on = 100)
WCA.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0, gamma = gammaWCA,r_cut=-1)
WCA.pair_coeff.set('A', 'C', epsilon=0.0, sigma=sigmaWCA, gamma = gammaWCA ,r_on = 100)
WCA.pair_coeff.set('A', 'D', epsilon=0.0, sigma=sigmaWCA, gamma = gammaWCA ,r_on = 100)
WCA.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
WCA.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0, gamma = gammaWCA ,r_cut=-1)
#########################

##############WCA
WCA2 = md.pair.slj(r_cut=2**(1.0/6), nlist=nlNANO,d_max = 4)
WCA2.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0,r_on = 100)
WCA2.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('A', 'C', epsilon=1.0, sigma=1.0,r_on = 100)
WCA2.pair_coeff.set('A', 'D', epsilon=1.0, sigma=1.0,r_on = 100)
WCA2.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)
WCA2.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0,r_cut=-1)
#########################




harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)

groupA = group.type(name='a-particles', type='A')
groupB = group.type(name='b-particles', type='B')
groupC = group.type(name='c-particles', type='C')
groupD = group.type(name='d-particles', type='D')


groupAB = group.union(name="ab-particles", a=groupA, b=groupB)  #diblocks
groupCD = group.union(name="cd-particles", a=groupC, b=groupD)  #triblocks
groupACD = group.union(name="acd-particles", a=groupA, b=groupCD) #polymers and centers
groupBCD = group.union(name="bcd-particles", a=groupB, b=groupCD) #polymers and centers
groupABCD = group.union(name="abcd-particles", a=groupAB, b=groupCD) #all

###############Rigid Body
rigid = md.constrain.rigid();
rigid.set_param('A',types=['B']*62,positions=[(0.44504,0,1.9499),(-0.22252,0.38542,1.9499),(-0.22252,-0.38542,1.9499),(1.247,0,1.5637),(0.95524,0.80154,1.5637),(0.21654,1.228,1.5637),(-0.62349,1.0799,1.5637),(-1.1718,0.42649,1.5637),(-1.1718,-0.42649,1.5637),(-0.62349,-1.0799,1.5637),(0.21654,-1.228,1.5637),(0.95524,-0.80154,1.5637),(1.8019,0,0.86777),(1.5605,0.90097,0.86777),(0.90097,1.5605,0.86777),(1.1034e-16,1.8019,0.86777),(-0.90097,1.5605,0.86777),(-1.5605,0.90097,0.86777),(-1.8019,2.2067e-16,0.86777),(-1.5605,-0.90097,0.86777),(-0.90097,-1.5605,0.86777),(-3.3101e-16,-1.8019,0.86777),(0.90097,-1.5605,0.86777),(1.5605,-0.90097,0.86777),(2,0,1.2246e-16),(1.8019,0.86777,1.2246e-16),(1.247,1.5637,1.2246e-16),(0.44504,1.9499,1.2246e-16),(-0.44504,1.9499,1.2246e-16),(-1.247,1.5637,1.2246e-16),(-1.8019,0.86777,1.2246e-16),(-2,2.4493e-16,1.2246e-16),(-1.8019,-0.86777,1.2246e-16),(-1.247,-1.5637,1.2246e-16),(-0.44504,-1.9499,1.2246e-16),(0.44504,-1.9499,1.2246e-16),(1.247,-1.5637,1.2246e-16),(1.8019,-0.86777,1.2246e-16),(1.8019,0,-0.86777),(1.5605,0.90097,-0.86777),(0.90097,1.5605,-0.86777),(1.1034e-16,1.8019,-0.86777),(-0.90097,1.5605,-0.86777),(-1.5605,0.90097,-0.86777),(-1.8019,2.2067e-16,-0.86777),(-1.5605,-0.90097,-0.86777),(-0.90097,-1.5605,-0.86777),(-3.3101e-16,-1.8019,-0.86777),(0.90097,-1.5605,-0.86777),(1.5605,-0.90097,-0.86777),(1.247,0,-1.5637),(0.95524,0.80154,-1.5637),(0.21654,1.228,-1.5637),(-0.62349,1.0799,-1.5637),(-1.1718,0.42649,-1.5637),(-1.1718,-0.42649,-1.5637),(-0.62349,-1.0799,-1.5637),(0.21654,-1.228,-1.5637),(0.95524,-0.80154,-1.5637),(0.44504,0,-1.9499),(-0.22252,0.38542,-1.9499),(-0.22252,-0.38542,-1.9499)]) 
rigid.validate_bodies()


#Ext=md.external.periodic()
#Ext.force_coeff.set('A', A=1.0, i=0, w=0.02, p=3)
#Ext.force_coeff.set(['B','C','D'], A=-0.1, i=0, w=0.02, p=3)

#periodic = md.external.periodic()
#periodic.force_coeff.set('A', A=1000.0, i=0, w=0.02, p=1)
#periodic.force_coeff.set('B', A=0.0, i=0, w=0.2, p=1)
#periodic.force_coeff.set('C', A=0.0, i=0, w=0.2, p=1)

if restart_flag==0:
	integrate=md.integrate.mode_standard(dt=0.02)
	nve=md.integrate.nve(group=groupACD)
	run(mix_time_steps) #mixup at beginning
	xmlPCNDstart = deprecated.dump.xml(group=group.all(), filename=prefix+"_start.xml", all=True)
	PCNDDCD=dump.dcd(filename=prefix+"_PCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	PCNDgsd_restart = dump.gsd(filename=prefix+'_PCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_cgcmm_energy','pressure' ],period=100, header_prefix='#', phase=0)
	zeroer= md.update.zero_momentum(period=1)
	PCND = cgcmm.angle.cgcmm()
	for i in range(numnano):
		print(i)
		PCND.set_coeff('Nanoparticle' + str(i+1), k = XiNano, t0 =TauNano, exponents ='individual',epsilon=numnano+numpoly,sigma=i) 
	for i in range(numpoly):
		print(i)
		PCND.set_coeff('Polymer' + str(i+1), k = XiPoly, t0 =TauPoly, exponents ='individual',epsilon=numnano+numpoly,sigma=i+numnano) 
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0)
	dpd.pair_coeff.set('A', 'B', A=0)
	dpd.pair_coeff.set('A', 'C', A=0)
	dpd.pair_coeff.set('A', 'D', A=0)
	dpd.pair_coeff.set('B', 'B', A=0)
	dpd.pair_coeff.set('B', 'C', A=0)
	dpd.pair_coeff.set('B', 'D', A=0)
	dpd.pair_coeff.set('C', 'C', A=25)
	dpd.pair_coeff.set('C', 'D', A=alphaAB)
	dpd.pair_coeff.set('D', 'D', A=25)
	#####################################################
	run_upto(totaltimesteps+mix_time_steps)
	xmlPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_PCND1final.xml", all=True)
	PCNDgsd_restart.disable()
	PCNDDCD.disable()
	PCND.disable()
	AfterPCND_gsd_restart = dump.gsd(filename=prefix+'_AfterPCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	AfterPCNDDCD=dump.dcd(filename=prefix+"_AfterPCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	run_upto(totaltimesteps+mix_time_steps+timesteps_afterPCND)
	xmlAfterPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_AfterPCND1final.xml", all=True)
elif restart_flag==1:
	integrate=md.integrate.mode_standard(dt=0.02)
	nve=md.integrate.nve(group=groupACD)
	PCNDDCD=dump.dcd(filename=prefix+"_PCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	PCNDgsd_restart = dump.gsd(filename=prefix+'_PCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_cgcmm_energy','pressure' ],period=100, header_prefix='#', phase=0)
	zeroer= md.update.zero_momentum(period=1)
	PCND = cgcmm.angle.cgcmm()
	for i in range(numnano):
		print(i)
		PCND.set_coeff('Nanoparticle' + str(i+1), k = XiNano, t0 =TauNano, exponents ='individual',epsilon=numnano+numpoly,sigma=i) 
	for i in range(numpoly):
		print(i)
		PCND.set_coeff('Polymer' + str(i+1), k = XiPoly, t0 =TauPoly, exponents ='individual',epsilon=numnano+numpoly,sigma=i+numnano) 
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0)
	dpd.pair_coeff.set('A', 'B', A=0)
	dpd.pair_coeff.set('A', 'C', A=0)
	dpd.pair_coeff.set('A', 'D', A=0)
	dpd.pair_coeff.set('B', 'B', A=0)
	dpd.pair_coeff.set('B', 'C', A=0)
	dpd.pair_coeff.set('B', 'D', A=0)
	dpd.pair_coeff.set('C', 'C', A=25)
	dpd.pair_coeff.set('C', 'D', A=alphaAB)
	dpd.pair_coeff.set('D', 'D', A=25)
	#####################################################
	run_upto(totaltimesteps+mix_time_steps)
	xmlPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_PCND1final.xml", all=True)
	PCNDgsd_restart.disable()
	PCNDDCD.disable()
	PCND.disable()
	AfterPCND_gsd_restart = dump.gsd(filename=prefix+'_AfterPCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	AfterPCNDDCD=dump.dcd(filename=prefix+"_AfterPCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	run_upto(totaltimesteps+mix_time_steps+timesteps_afterPCND)
	xmlAfterPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_AfterPCND1final.xml", all=True)
elif restart_flag==2:
	integrate=md.integrate.mode_standard(dt=0.02)
	nve=md.integrate.nve(group=groupACD)
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0)
	dpd.pair_coeff.set('A', 'B', A=0)
	dpd.pair_coeff.set('A', 'C', A=0)
	dpd.pair_coeff.set('A', 'D', A=0)
	dpd.pair_coeff.set('B', 'B', A=0)
	dpd.pair_coeff.set('B', 'C', A=0)
	dpd.pair_coeff.set('B', 'D', A=0)
	dpd.pair_coeff.set('C', 'C', A=25)
	dpd.pair_coeff.set('C', 'D', A=alphaAB)
	dpd.pair_coeff.set('D', 'D', A=25)
	#####################################################
	AfterPCND_gsd_restart = dump.gsd(filename=prefix+'_AfterPCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	AfterPCNDDCD=dump.dcd(filename=prefix+"_AfterPCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	run_upto(totaltimesteps+mix_time_steps+timesteps_afterPCND)
	xmlAfterPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_AfterPCND1final.xml", all=True)
elif restart_flag==3:
	integrate=md.integrate.mode_standard(dt=0.02)
	nve=md.integrate.nve(group=groupACD)
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0)
	dpd.pair_coeff.set('A', 'B', A=0)
	dpd.pair_coeff.set('A', 'C', A=0)
	dpd.pair_coeff.set('A', 'D', A=0)
	dpd.pair_coeff.set('B', 'B', A=0)
	dpd.pair_coeff.set('B', 'C', A=0)
	dpd.pair_coeff.set('B', 'D', A=0)
	dpd.pair_coeff.set('C', 'C', A=25)
	dpd.pair_coeff.set('C', 'D', A=alphaAB)
	dpd.pair_coeff.set('D', 'D', A=25)
	#####################################################
	AfterPCND_gsd_restart = dump.gsd(filename=prefix+'_AfterPCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
	AfterPCNDDCD=dump.dcd(filename=prefix+"_AfterPCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
	run_upto(totaltimesteps+mix_time_steps+timesteps_afterPCND)
	xmlAfterPCNDfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_AfterPCND1final.xml", all=True)
