% addpath('/media/andrew/External_Data/Google Drive LATECH/Diblock_Blending/SMMA_BDEO_match/SMMAvaryfrac')

clear all
fclose all
clc
global m t k j timestep StructString knano oldknano oldk oldt kpoly tnano tpoly stepsize lengthgrafts numA maxdia numgrafts numnano numpoly rigidstring ks ts  ksnano kspoly tsnano tspoly filename fid1 fid2 startxmlname PCNDtemplate NoPCNDtemplate profiletemplate alphaAB timesteps_afterPCND totaltimesteps mix_time_steps dcd_period r_buffDPD r_buffNANO modifiername

Domain=14.5;
repeats=2;
StructType='lam';
maxdia=4 %%%%%%%%%%%%%%%% 2*r
numnano=264 %%%%%%%%%%%%%%%%%
numpoly=2640%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numgrafts=10 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
numA=9 %%%%%%%%%%%%%%%%%%%%%%
lengthgrafts=18 %%%%%%%%%%%%%%%%%
LONIpath=['/work/apeters/' 'Nr2_numgrafts6_lengthgrafts18_fracA_0p5' '/lam']; %%%%%%%%%%%%%%%%
StructString=[StructType '_D' num2str(Domain) '_rep' num2str(repeats)];
rigidstring=['rigid.set_param(''A'',types=[''B'']*11,positions=[(1,0,1.7321),(-0.5,0.86603,1.7321),(-0.5,-0.86603,1.7321),(2,0,1.2246e-16),(0.61803,1.9021,1.2246e-16),(-1.618,1.1756,1.2246e-16),(-1.618,-1.1756,1.2246e-16),(0.61803,-1.9021,1.2246e-16),(1,0,-1.7321),(-0.5,0.86603,-1.7321),(-0.5,-0.86603,-1.7321)]) \n']

if mod(maxdia/2,1)==0
    startxmlname = ['Make_PCNDNano' num2str(numnano) '_r' num2str(floor(maxdia/2)) '_N' num2str(lengthgrafts) 'NA' num2str(numA) '_nG' num2str(numgrafts) StructString '_start.xml'];
else
    rstr=num2str(mod(maxdia/2,1));
    rstr=rstr(3:end);
    startxmlname = ['Make_PCNDNano' num2str(numnano) '_r' num2str(floor(maxdia/2)) 'p' rstr '_N' num2str(lengthgrafts) 'NA' num2str(numA) '_nG' num2str(numgrafts) StructString '_start.xml'];
end
%startxmlname = 'Makestart200_r3_start.xml' %%%%%%%%%%%%%%%%%%%%%%%%
modifiername='2p5m';
ks=[];
ts=[];
PCNDtemplate = 'PCND_nano_template2.py'
NoPCNDtemplate = 'NoPCND_nano_template2.py'
profiletemplate = 'profile_nano_template3.py'
alphaAB = 100
timesteps_afterPCND = 500000
totaltimesteps = 5000000
mix_time_steps = 10000
dcd_period = 10000
r_buffDPD = 0.5
r_buffNANO = 4.0
timestep=0.01;


%F=dir('/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr0p5_numgrafts2_lengthgrafts20_fracA_0p5/');
walltime=72 %hours


%Write normals
counter=0;
for m = 1:length(ts)
    t=ts(m);
    for j=1:length(ks)
        counter=counter+1;
        k=ks(j);
        if (isempty(modifiername)) || sum(modifiername==0)
            filename=['PCND_Nano_Xi' num2str(k) '_t' num2str(t) '_' StructString '_'];
        else
            filename=['PCND_Nano_Xi' num2str(k) '_t' num2str(t) '_' StructString '_' modifiername];
        end
        
        writenormalhoo()
        writepbs(filename,LONIpath,walltime)
        if mod(counter,2)==0
            writedoublepbs(oldfilename,filename,oldLONIpath,LONIpath,walltime)
        end
        oldfilename=filename;
        oldLONIpath=LONIpath;
        oldk=k;
        oldt=t;
    end
end


%Write No PCND
if (isempty(modifiername)) || sum(modifiername==0)
    filename = ['PCND_Nano_NoPCND_D' num2str(Domain) '_nG' num2str(numgrafts)];
else
    filename = ['PCND_Nano_NoPCND_D' num2str(Domain) '_nG' num2str(numgrafts) '_' modifiername];
end
k=0;
writeNoPCNDhoo()
writepbs(filename,LONIpath,walltime)

%write profilers
filename = ['PCND_LONI_Profiler']
writeProfilehoo()
writepbs(filename,LONIpath,walltime)
filename = ['PCND_TITAN_Profiler']
writeProfilehoo()
writepbs(filename,LONIpath,walltime)

fclose all



    
function []=writepbs(prefix,LONIpath,walltime)
    global t k
%     prefix=file;
    pyname=[prefix '.py'];
    shortprefix=prefix(end-14:end);
    shortprefix=['X' num2str(k) 'T' num2str(t) prefix(end-14:end)];
    pbsname=[prefix '.pbs'];
    fid=fopen(pbsname,'w','l')


    fprintf(fid,'#!/bin/bash\n');
    fprintf(fid,['#PBS -l walltime=' num2str(walltime) ':00:00,nodes=1:ppn=20\n']);
    fprintf(fid,['#PBS -N ' shortprefix '\n']);
    fprintf(fid,['#PBS -o ' shortprefix '_end.o' '\n']);
    fprintf(fid,['#PBS -e ' shortprefix '_end.e' '\n']);
    fprintf(fid,['#PBS -q checkpt \n']);
    fprintf(fid,'#PBS -m abe\n');
    fprintf(fid,'#PBS -M apeters@latech.edu\n');
    fprintf(fid,'\n');
    fprintf(fid,['cd ' LONIpath '\n']);
    fprintf(fid,'module load cuda/8.0 \n');
%         fprintf(fid,'export SOFTWARE_ROOT=~/hoomd-install\n');
%         fprintf(fid,'export PYTHONPATH=$PYTHONPATH:${SOFTWARE_ROOT}/lib/python\n');
    fprintf(fid,['export HOOMD_WALLTIME_STOP=$((`date +%%s` +  (' num2str(walltime) '*60*60)-(3*60)))' '\n']);
    fprintf(fid,['python ' pyname ' --mode=gpu --msg-file=''' prefix '.o''\n' ]);
end

function []=writedoublepbs(oldprefix,prefix,oldLONIpath,LONIpath,walltime)
    global k oldk t oldt
%     prefix=file;
    pyname=[prefix '.py'];
    oldpyname=[oldprefix '.py'];
    
    shortprefix=prefix(end-6:end);
    oldshortprefix=oldprefix(end-6:end);
    combshortprefix=['k' num2str(oldk) 't' num2str(oldt) '_k' num2str(k) 't' num2str(t)];
    
    pbsname=[oldprefix '_' prefix '.pbs'];
    fid=fopen(pbsname,'w','l')


    fprintf(fid,'#!/bin/bash\n');
    fprintf(fid,['#PBS -l walltime=' num2str(walltime) ':00:00,nodes=1:ppn=20\n']);
    fprintf(fid,['#PBS -N ' combshortprefix '\n']);
    fprintf(fid,['#PBS -o ' combshortprefix '_end.o' '\n']);
    fprintf(fid,['#PBS -e ' combshortprefix '_end.e' '\n']);
    fprintf(fid,['#PBS -q checkpt \n']);
    fprintf(fid,'#PBS -m abe\n');
    fprintf(fid,'#PBS -M apeters@latech.edu\n');
    fprintf(fid,'\n');
    %fprintf(fid,['cd ' LONIpath '\n']);
    fprintf(fid,'module load cuda/8.0 \n');
%         fprintf(fid,'export SOFTWARE_ROOT=~/hoomd-install\n');
%         fprintf(fid,'export PYTHONPATH=$PYTHONPATH:${SOFTWARE_ROOT}/lib/python\n');
    fprintf(fid,['export HOOMD_WALLTIME_STOP=$((`date +%%s` +  (' num2str(walltime) '*60*60)-(3*60)))' '\n']);
    fprintf(fid,['cd ' oldLONIpath ' && python ' oldpyname ' --gpu=0 --msg-file=''' oldprefix '.o'' & cd ' LONIpath ' && python ' pyname ' --gpu=1 --msg-file=''' prefix '.o''\n']);
end

function []=writenormalhoo()
global m t k j timestep maxdia numnano numpoly rigidstring ks ts filename fid1 fid2 startxmlname PCNDtemplate NoPCNDtemplate profiletemplate alphaAB timesteps_afterPCND totaltimesteps mix_time_steps dcd_period r_buffDPD r_buffNANO

    fid1=fopen(PCNDtemplate,'r');
    fid2=fopen([filename '.py'],'w','l');
    for i=1:15
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end

      x=fgetl(fid1);
      fprintf(fid2,['prefix=''' filename '''\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Xi=' num2str(k) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Tau=' num2str(t) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['alphaAB=' num2str(alphaAB) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['totaltimesteps=' num2str(totaltimesteps) '\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['timesteps_afterPCND=' num2str(timesteps_afterPCND) '\n']);

        x=fgetl(fid1);
      fprintf(fid2,['numnano= ' num2str(numnano) ';\n']);

      x=fgetl(fid1);
      fprintf(fid2,['numpoly= ' num2str(numpoly) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['mix_time_steps= ' num2str(mix_time_steps) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['dcd_period= ' num2str(dcd_period) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['partradius= ' num2str(maxdia/2) ';\n']);
      


        for i=27:41
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
        x=fgetl(fid1);
      fprintf(fid2,['	system=deprecated.init.read_xml(filename="' startxmlname '",time_step=0)\n']);
        
        for i=43:45
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
        x=fgetl(fid1);
      fprintf(fid2,['nlDPD = md.nlist.cell(r_buff=' num2str(r_buffDPD) ');\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['nlNANO = md.nlist.tree(r_buff=' num2str(r_buffNANO) ');\n']);
        
        for i=48:80
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end


        x=fgetl(fid1);
      fprintf(fid2,['WCA2 = md.pair.slj(r_cut=2**(1.0/6), nlist=nlNANO,d_max = ' num2str(maxdia) ')\n']);

      for i=82:113
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end

        x=fgetl(fid1);
      fprintf(fid2,rigidstring);

      for i=115:236
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
      end
      
      fclose(fid1);
      fclose(fid2);

      if exist('timestep','var')
          find_and_replace([filename '.py'],'dt=0.02',['dt=' num2str(timestep)]);
      end
end

function []=writeNoPCNDhoo()
global m t k j maxdia timestep numnano numpoly rigidstring ks ts filename fid1 fid2 startxmlname PCNDtemplate NoPCNDtemplate profiletemplate alphaAB timesteps_afterPCND totaltimesteps mix_time_steps dcd_period r_buffDPD r_buffNANO

    fid1=fopen(NoPCNDtemplate,'r');
    fid2=fopen([filename '.py'],'w','l');
    for i=1:15
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end

      x=fgetl(fid1);
      fprintf(fid2,['prefix=''' filename '''\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Xi=' num2str(k) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Tau=' num2str(t) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['alphaAB=' num2str(alphaAB) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['totaltimesteps=' num2str(totaltimesteps) '\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['timesteps_afterPCND=' num2str(timesteps_afterPCND) '\n']);

        x=fgetl(fid1);
      fprintf(fid2,['numnano= ' num2str(numnano) ';\n']);

      x=fgetl(fid1);
      fprintf(fid2,['numpoly= ' num2str(numpoly) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['mix_time_steps= ' num2str(mix_time_steps) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['dcd_period= ' num2str(dcd_period) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['partradius= ' num2str(maxdia/2) ';\n']);


        for i=27:41
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
        x=fgetl(fid1);
      fprintf(fid2,['	system=deprecated.init.read_xml(filename="' startxmlname '",time_step=0)\n']);
        
        for i=43:45
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
        x=fgetl(fid1);
      fprintf(fid2,['nlDPD = md.nlist.cell(r_buff=' num2str(r_buffDPD) ');\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['nlNANO = md.nlist.tree(r_buff=' num2str(r_buffNANO) ');\n']);
        
        for i=48:80
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end


        x=fgetl(fid1);
      fprintf(fid2,['WCA2 = md.pair.slj(r_cut=2**(1.0/6), nlist=nlNANO,d_max = ' num2str(maxdia) ')\n']);

      for i=82:113
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end

        x=fgetl(fid1);
      fprintf(fid2,rigidstring);

      for i=115:218
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
      end

      
      fclose(fid1);
      fclose(fid2);

      if exist('timestep','var')
          find_and_replace([filename '.py'],'dt=0.02',['dt=' num2str(timestep)]);
      end
end

function []=writeProfilehoo()
global m t k j maxdia timestep numnano numpoly rigidstring ks ts filename fid1 fid2 startxmlname PCNDtemplate NoPCNDtemplate profiletemplate alphaAB timesteps_afterPCND totaltimesteps mix_time_steps dcd_period r_buffDPD r_buffNANO

    fid1=fopen(profiletemplate,'r');
    fid2=fopen([filename '.py'],'w','l');
    for i=1:15
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end

      x=fgetl(fid1);
      fprintf(fid2,['prefix=''' filename '''\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Xi=' num2str(k) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['Tau=' num2str(t) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['alphaAB=' num2str(alphaAB) '\n']);

      x=fgetl(fid1);
      fprintf(fid2,['totaltimesteps=' num2str(totaltimesteps) '\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['timesteps_afterPCND=' num2str(timesteps_afterPCND) '\n']);

        x=fgetl(fid1);
      fprintf(fid2,['numnano= ' num2str(numnano) ';\n']);

      x=fgetl(fid1);
      fprintf(fid2,['numpoly= ' num2str(numpoly) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['mix_time_steps= ' num2str(mix_time_steps) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['dcd_period= ' num2str(dcd_period) ';\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['partradius= ' num2str(maxdia/2) ';\n']);


        for i=27:30
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
        x=fgetl(fid1);
      fprintf(fid2,['system=deprecated.init.read_xml(filename="' startxmlname '",time_step=0)\n']);
        
        for i=32:68
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end

        x=fgetl(fid1);
      fprintf(fid2,['WCA2 = md.pair.slj(r_cut=2**(1.0/6), nlist=nlNANO,d_max = ' num2str(maxdia) ')\n']);

      for i=70:101
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end

        x=fgetl(fid1);
      fprintf(fid2,rigidstring);

      for i=103:152
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
      end

      
      fclose(fid1);
      fclose(fid2);

      if exist('timestep','var')
          find_and_replace([filename '.py'],'dt=0.02',['dt=' num2str(timestep)]);
      end
end

          