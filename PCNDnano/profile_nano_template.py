
#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
from hoomd import cgcmm
import sys
import numpy
import calendar
import time
import math
from random import *
c = context.initialize()

prefix='verifyn800profilesten'
Xi=0.01
Tau=100
alphaAB=100
totaltimesteps=2000000
timesteps_afterPCND=1000000
numnano=800;
numpoly=8023;
mix_time_steps=10000
dcd_period=1000
option.set_msg_file(prefix+'.o')

system=deprecated.init.read_xml(filename="MixedN800.xml",time_step=0)

#########Set up forcefields and integrators
nlDPD = md.nlist.cell(r_buff=2)
nlNANO = md.nlist.tree(r_buff=2)
c.sorter.set_params(grid=2)
c.sorter.disable()
#######DPD
dpd = md.pair.dpd(r_cut=1.0, nlist=nlDPD, kT=1.0, seed=calendar.timegm(time.gmtime())) 
dpd.pair_coeff.set('A', 'A', A=0, gamma = 3,r_cut=False)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 3,r_cut=False)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 3,r_cut=False)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 3,r_cut=False)


dpd.pair_coeff.set('B', 'B', A=0, gamma = 3,r_cut=False)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 3,r_cut=False)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 3,r_cut=False)

dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
dpd.pair_coeff.set('C', 'D', A=25, gamma = 3)

dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)

##############WCA
WCA = md.pair.slj(r_cut=2**(1.0/6), nlist=nlNANO,d_max = 1.5)
WCA.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0,r_on = 10)
WCA.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0,r_cut=False)
WCA.pair_coeff.set('A', 'C', epsilon=1.0, sigma=1.0,r_on = 10)
WCA.pair_coeff.set('A', 'D', epsilon=1.0, sigma=1.0,r_on = 10)

WCA.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0,r_cut=False)
WCA.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0,r_cut=False)
WCA.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0,r_cut=False)

WCA.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0,r_cut=False)
WCA.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0,r_cut=False)

WCA.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0,r_cut=False)
#########################




harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)

groupA = group.type(name='a-particles', type='A')
groupB = group.type(name='b-particles', type='B')
groupC = group.type(name='c-particles', type='C')
groupD = group.type(name='d-particles', type='D')


groupAB = group.union(name="ab-particles", a=groupA, b=groupB)  #diblocks
groupCD = group.union(name="cd-particles", a=groupC, b=groupD)  #triblocks
groupACD = group.union(name="acd-particles", a=groupA, b=groupCD) #polymers and centers
groupBCD = group.union(name="bcd-particles", a=groupB, b=groupCD) #polymers and centers
groupABCD = group.union(name="abcd-particles", a=groupAB, b=groupCD) #all

###############Rigid Body
rigid = md.constrain.rigid();
rigid.set_param('A',types=['B']*12,positions=[ (0.75,0,1.299),(-0.375,0.64952,1.299),(-0.375,-0.64952,1.299),(1.5,0,9.1849e-17),(0.75,1.299,9.1849e-17),(-0.75,1.299,9.1849e-17),(-1.5,1.837e-16,9.1849e-17),(-0.75,-1.299,9.1849e-17),(0.75,-1.299,9.1849e-17),(0.75,0,-1.299),(-0.375,0.64952,-1.299),(-0.375,-0.64952,-1.299)])
rigid.validate_bodies()


#Ext=md.external.periodic()
#Ext.force_coeff.set('A', A=1.0, i=0, w=0.02, p=3)
#Ext.force_coeff.set(['B','C','D'], A=-0.1, i=0, w=0.02, p=3)

#periodic = md.external.periodic()
#periodic.force_coeff.set('A', A=1000.0, i=0, w=0.02, p=1)
#periodic.force_coeff.set('B', A=0.0, i=0, w=0.2, p=1)
#periodic.force_coeff.set('C', A=0.0, i=0, w=0.2, p=1)


integrate=md.integrate.mode_standard(dt=0.02)
nve=md.integrate.nve(group=groupACD)
run(mix_time_steps) #mixup at beginning
xmlPCNDstart = deprecated.dump.xml(group=group.all(), filename=prefix+"_start.xml", all=True)
PCNDDCD=dump.dcd(filename=prefix+"_PCND1traj.dcd", group=groupABCD, period = dcd_period, phase=0)
PCNDgsd_restart = dump.gsd(filename=prefix+'_PCND1restart.gsd', group=group.all(), truncate=True, period = dcd_period, phase=0)
analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_cgcmm_energy','pressure' ],period=100, header_prefix='#', phase=0)
zeroer= md.update.zero_momentum(period=1)
PCND = cgcmm.angle.cgcmm()
for i in range(numnano):
        print(i)
        PCND.set_coeff('Nanoparticle' + str(i+1), k = Xi, t0 =Tau, exponents ='individual',epsilon=numnano+numpoly,sigma=i) 
for i in range(numpoly):
        print(i)
        PCND.set_coeff('Polymer' + str(i+1), k = Xi, t0 =Tau, exponents ='individual',epsilon=numnano+numpoly,sigma=i+numnano) 
###################################################
dpd.pair_coeff.set('A', 'A', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 3)
dpd.pair_coeff.set('B', 'B', A=0, gamma = 3)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 3)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 3)
dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 3)
dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)
#####################################################
run(5000,profile=True)
nlDPD.tune(warmup=5000, r_min=0.05, r_max=3.0, jumps=10, steps=1000, set_max_check_period=True, quiet=False)
run(5000,profile=True)
nlDPD.tune(warmup=10000, r_min=0.05, r_max=3.0, jumps=30, steps=5000, set_max_check_period=True, quiet=False)
run(10000,profile=True)

nlNANO.tune(warmup=5000, r_min=0.05, r_max=3.0, jumps=10, steps=1000, set_max_check_period=True, quiet=False)
run(5000,profile=True)
nlNANO.tune(warmup=10000, r_min=0.05, r_max=3.0, jumps=30, steps=5000, set_max_check_period=True, quiet=False)
run(10000,profile=True)
