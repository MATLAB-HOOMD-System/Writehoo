function writedoublepbs_QBC(varargin)

%writedoublepbs_QBC('queue','gpu','wallclocktime',12)
p=inputParser;

path='~/MATLAB/pbs/'
files=dir(path);
if length(files)<4;
    defaultpbs1='placeholder';
    defaultpbs2=defaultpbs1
else
    defaultpbs1=files(3).name;
    defaultpbs2=files(4).name;
end

addParameter(p,'pbs1',defaultpbs1)
addParameter(p,'pbs2',defaultpbs2)
addParameter(p,'pbsname','default')
addParameter(p,'queue','empty')
addParameter(p,'wallclocktime',0)
addParameter(p,'allocation','empty')
addParameter(p,'firststring','empty')


parse(p,varargin{:});
fields=fieldnames(p.Results);
for i=1:numel(fields)
    eval([fields{i} '=' 'p.Results.' fields{i} ';']);
end



if exist('pbs1','var')~=2
    path='~/MATLAB/pbs/'
    files=dir(path);
%     filenames=makefilenames(files)
    if length(files)<4
        error('files not specifed properly');
    else 
        if mod(length(files),2)~=0
            error('odd number of files');
        end
        
        %% sort files
        if ~strcmp(firststring,'empty')
            for i=4:length(files)
                if ~isempty(findstr(files(i).name,firststring)) %if it finds the string
                    tempstr=files(i).name;
                    for j=3:i-1
                        tempstrs{j}=files(j).name;
                    end
                    for j=4:i
                        files(j).name=tempstrs{j-1};
                    end
                    files(3).name=tempstr;
                end
            end
        end
%         filenames2=makefilenames(files)
        %%
        for i=3:2:length(files)
            pbs1=[path files(i).name];
            pbs2=[path files(i+1).name];
            writeboth(pbs1,pbs2,pbsname,queue,wallclocktime,allocation)
        end
    end

end

end
    
function filenames=makefilenames(files)
    for i=1:length(files)
        filenames{i}=files(i).name;
    end
end

function writeboth(pbs1,pbs2,pbsname,queue,wallclocktime,allocation)
fid1=fopen(pbs1,'r');
fid2=fopen(pbs2,'r');

testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'#SBATCH -t '));
        testpython=1;
    end
end
xnum=strfind(x,'#SBATCH -t ');
walltime1=str2num(x(xnum+12:xnum+12+1));

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'#SBATCH -t '));
        testpython=1;
    end
end
xnum=strfind(x,'#SBATCH -t ');
walltime2=str2num(x(xnum+12:xnum+12+1));
walltime=max([walltime1 walltime2]);

testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,' -p '));
        testpython=1;
    end
end
xnum=strfind(x,' -p ');
if strcmp(queue,'empty');
    queue= x(12:end);
end


testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'cd '));
        testpython=1;
    end
end
path1=x(4:end);

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'cd '));
        testpython=1;
    end
end
path2=x(4:end);


testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'export HOOMD_WALLTIME_STOP'));
        testpython=1;
    end
end
walltimestopline=x;



testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'python '));
        testpython=1;
    end
end
runstring1=x;
pystring1=x(8:strfind(x,'.py ')+3);

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'python '));
        testpython=1;
    end
end
runstring2=x;
pystring2=x(8:strfind(x,'.py ')+3);

%check pbswritefile, update, and change pbsname if necessary
if exist('doublepbswritefile.mat','file')~=2
    day=datetime('today');
    pbscounter=1;
    save('doublepbswritefile.mat','day','pbscounter')
end
load('doublepbswritefile.mat')
if datetime('today')~=day;
    day=datetime('today');
    pbscounter=1;
end
if pbsname=='default' 
    pbsname=[datestr(day) '-' num2str(pbscounter)];
end
pbscounter=pbscounter+1;
save('doublepbswritefile.mat','day','pbscounter')
if wallclocktime~=0
    walltime=wallclocktime;
end
    

fid=fopen([pbsname '.pbs'],'w','l')

nodes=1;
fprintf(fid,'#!/bin/bash\n');
fprintf(fid,['#SBATCH -N ' num2str(nodes) '\n']);
fprintf(fid,['#SBATCH -n 48\n']);
fprintf(fid,['#SBATCH -t ' num2str(wallclocktime) ':00:00\n']);
fprintf(fid,['#SBATCH -p ' queue '\n']);
fprintf(fid,['#SBATCH --output="' pbsname '_end.o"\n']);
fprintf(fid,'\n');
fprintf(fid,['\n']);
%fprintf(fid,'module load cuda/10.2.89 \n');
%         fprintf(fid,'export SOFTWARE_ROOT=~/hoomd-install\n');
%         fprintf(fid,'export PYTHONPATH=$PYTHONPATH:${SOFTWARE_ROOT}/lib/python\n');
fprintf(fid,['export HOOMD_WALLTIME_STOP=$((`date +%%s` +  (' num2str(walltime) '*60*60)-(3*60)))' '\n']);

str=['cd ' path1 ' && python ' pystring1 ' --gpu=0 --msg-file=''' pystring1(1:end-3) 'o'' & cd ' path2 ' && python ' pystring2 ' --gpu=1 --msg-file=''' pystring2(1:end-3) 'o''\n\n'];
fprintf(fid,str)
fprintf(fid,'wait\n');
fclose all
end
