addpath('/media/andrew/External_Data/Google Drive LATECH/Diblock_Blending/SMMA_BDEO_match/SMMAvaryfrac')

clear all
fclose all
clc
volfracs=0:0.1:1;
   
for j=1:length(volfracs)
    volfrac=volfracs(j)
    filename=['negativeACchi_N4_SMMABDEO_' num2str(volfrac)]
    fid1=fopen('SMMBDEO_varyvol_template.py','r');
    fid2=fopen([filename '.py'],'w','l');
 

    for i=1:13
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end
    
      x=fgetl(fid1);
      fprintf(fid2,['volfrac=' num2str(volfrac) '\n']);
          
        for i=15:79
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
end
fclose all
          