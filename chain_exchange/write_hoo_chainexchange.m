% addpath('/media/andrew/External_Data/Google Drive LATECH/Diblock_Blending/SMMA_BDEO_match/SMMAvaryfrac')

clear all
fclose all
clc
A1s=3:5
poly1fracs=0:0.1:1
for m = 1:length(A1s)
    A1=A1s(m);
    for j=1:length(poly1fracs)
        poly1frac=poly1fracs(j);
        A2=6;
        B1=12;
        B2=12;
        alphaAB=42.5;
        Boxsize=45;
        volfrac=0.05;
        totaltimesteps=4000000;
        
        filename=['A1_'   num2str(A1)   '_B1_'   num2str(B1)   '_A2_'   num2str(A2)   '_B2_'   num2str(B2)   '_frac1_'   num2str(poly1frac)   '_a'   num2str(alphaAB)   '_box'   num2str(Boxsize)]    
        fid1=fopen('CE_template.py','r');
        fid2=fopen([filename '.py'],'w','l');


        for i=1:14
                x=fgetl(fid1);
                fprintf(fid2,x);
                fprintf(fid2,'\n');
        end

          x=fgetl(fid1);
          fprintf(fid2,['alphaAB=' num2str(alphaAB) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['totaltimesteps=' num2str(totaltimesteps) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['A1=' num2str(A1) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['A2=' num2str(A2) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['B1=' num2str(B1) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['B2=' num2str(B2) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['Boxsize=' num2str(Boxsize) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['volfrac=' num2str(volfrac) '\n']);
          
          x=fgetl(fid1);
          fprintf(fid2,['fracpoly1=' num2str(poly1frac) '\n']);
          
          
            for i=24:81
                x=fgetl(fid1);
                if i ==35
                    x=strrep(x,'\n','\\n')
                end
                fprintf(fid2,x);
                fprintf(fid2,'\n');
            end

    end
end
fclose all
          