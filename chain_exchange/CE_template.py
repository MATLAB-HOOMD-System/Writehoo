#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
import sys

import numpy
import calendar
import time

c = context.initialize()


alphaAB=42.5
totaltimesteps=4000000
A1=3
A2=6
B1=12
B2=12
Boxsize=45
volfrac=0.05
fracpoly1=0.1

numpolybeads=round(Boxsize**3*volfrac*3)
numpoly1beads=round(fracpoly1*numpolybeads)
numpoly2beads=numpolybeads-numpoly1beads
poly1length=A1+B1
poly2length=A2+B2
numpoly1chains=round(numpoly1beads/poly1length)
numpoly2chains=round(numpoly2beads/poly2length)
wtfrac1=numpoly1chains*poly1length/(numpoly1chains*poly1length+numpoly2chains*poly2length)
numsolvent=Boxsize**3*3-numpoly1chains*poly1length-numpoly2chains*poly2length
prefix='A1_' + str(A1) + '_B1_' + str(B1) + '_A2_' + str(A2) + '_B2_' + str(B2) + '_frac1_' + str(fracpoly1) + '_a' + str(alphaAB) + '_box' + str(Boxsize)
print('\n\n wtfrac1= ',wtfrac1,'  #poly1chains = ',numpoly1chains, ' #poly2chains = ',numpoly2chains, '  totalbeads = ',numsolvent+ numpoly1chains*poly1length+numpoly2chains*poly2length, '  expected total beads = ', Boxsize**3*3, '\n\n')

#find restartfile
if os.path.isfile(prefix+'_restart.gsd'):
    init.read_gsd(filename=prefix+'_restart.gsd')
else:
    #build polymers
    polymer1 = dict(bond_len=1, type=['A']*A1 + ['B']*B1,bond="linear", count=numpoly1chains)
    polymer2 = dict(bond_len=1, type=['A']*A2 + ['B']*B2,bond="linear", count=numpoly2chains)
    solvent=dict(bond_len=1, type=['C'],bond="linear", count=numsolvent)
    system=deprecated.init.create_random_polymers(box=data.boxdim(L=Boxsize),polymers=[polymer1,polymer2, solvent],separation=dict(A=0.1, B=0.1, C=0.1));
    groupA = group.type(name='a-particles', type='A')
    groupB = group.type(name='b-particles', type='B')
    groupAB = group.union(name="ab-particles", a=groupA, b=groupB)
    xml = deprecated.dump.xml(group=group.all(), filename=prefix+"_start.xml", vis=True)
    xmlAB = deprecated.dump.xml(group=groupAB, filename=prefix+"_startAB.xml", position=True,type=True,diameter=True)

#########Set up forcefields and integrators
nl = md.nlist.cell()

dpd = md.pair.dpd(r_cut=1.0, nlist=nl, kT=1.0, seed=calendar.timegm(time.gmtime()))
dpd.pair_coeff.set('A', 'A', A=25, gamma = 3)
dpd.pair_coeff.set('A', 'B', A=alphaAB, gamma = 3)
dpd.pair_coeff.set('A', 'C', A=alphaAB, gamma = 3)
dpd.pair_coeff.set('B', 'B', A=25, gamma = 3)
dpd.pair_coeff.set('B', 'C', A=25, gamma = 3)
dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)

harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)

md.integrate.mode_standard(dt=0.04)
md.integrate.nve(group=group.all())

########set up analysis files and dumps
analyze.log(filename=prefix+'.log', quantities=['temperature', 'potential_energy','pair_dpd_energy', 'bond_harmonic_energy' ],period=100, header_prefix='#', phase=0)

#dump.dcd(filename=prefix+"_traj.dcd", group=groupAB, period=100, phase=0)
dump.dcd(filename=prefix+"_fasttraj.dcd", group=groupAB,period=1000, phase=0)
gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)

try:
    run_upto(totaltimesteps, limit_multiple=1000)
    xml = deprecated.dump.xml(group=group.all(), filename=prefix+"_final.xml", vis=True)
except WalltimeLimitReached:
    gsd_restart.write_restart()

