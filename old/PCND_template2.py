#! /usr/bin/env hoomd
from __future__ import print_function, division
from hoomd import *
from hoomd import md
from hoomd import deprecated
import sys
import numpy
import calendar
import time
import math
c = context.initialize()

prefix='PCNDnanotest_Xi10_t10000'
Xi=10
Tau=10000
alphaAB=25
totaltimesteps=1000000
timesteps_afterPCND=100000

if os.path.isfile(prefix+'_PCND1final.xml'):
	system=deprecated.init.read_xml(filename=prefix+'_PCND1final.xml')
	restart_flag=2
elif os.path.isfile(prefix+'_restart.gsd'):
	init.read_gsd(filename=prefix+'_restart.gsd')
	#gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
	restart_flag=1
else:
	system=deprecated.init.read_xml(filename="PCNDnanotest_mixedstart.xml")
	#init.read_gsd(filename='PCNDnanotest_mixed.gsd')
	#gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
	restart_flag=0

#########Set up forcefields and integrators
nl = md.nlist.cell()
c.sorter.disable()
#######DPD
dpd = md.pair.dpd(r_cut=1.0, nlist=nl, kT=1.0, seed=calendar.timegm(time.gmtime()))
dpd.pair_coeff.set('A', 'A', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'B', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'C', A=0, gamma = 3)
dpd.pair_coeff.set('A', 'D', A=0, gamma = 3)


dpd.pair_coeff.set('B', 'B', A=0, gamma = 3)
dpd.pair_coeff.set('B', 'C', A=0, gamma = 3)
dpd.pair_coeff.set('B', 'D', A=0, gamma = 3)

dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 3)

dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)

##############WCA
WCA = md.pair.slj(r_cut=2**(1.0/6), nlist=nl,d_max = 5.0)
WCA.pair_coeff.set('A', 'A', epsilon=1.0, sigma=1.0,r_on = 10)
WCA.pair_coeff.set('A', 'B', epsilon=0.0, sigma=1.0)
WCA.pair_coeff.set('A', 'C', epsilon=1.0, sigma=1.0,r_on = 10)
WCA.pair_coeff.set('A', 'D', epsilon=1.0, sigma=1.0,r_on = 10)

WCA.pair_coeff.set('B', 'B', epsilon=0.0, sigma=1.0)
WCA.pair_coeff.set('B', 'C', epsilon=0.0, sigma=1.0)
WCA.pair_coeff.set('B', 'D', epsilon=0.0, sigma=1.0)

WCA.pair_coeff.set('C', 'C', epsilon=0.0, sigma=1.0)
WCA.pair_coeff.set('C', 'D', epsilon=0.0, sigma=1.0)

WCA.pair_coeff.set('D', 'D', epsilon=0.0, sigma=1.0)
#########################




harmonic = md.bond.harmonic(name="mybond")
harmonic.bond_coeff.set('polymer', k=100, r0=1)

groupA = group.type(name='a-particles', type='A')
groupB = group.type(name='b-particles', type='B')
groupC = group.type(name='c-particles', type='C')
groupD = group.type(name='d-particles', type='D')


groupAB = group.union(name="ab-particles", a=groupA, b=groupB)  #diblocks
groupCD = group.union(name="cd-particles", a=groupC, b=groupD)  #triblocks
groupACD = group.union(name="acd-particles", a=groupA, b=groupCD) #polymers and centers
groupBCD = group.union(name="bcd-particles", a=groupB, b=groupCD) #polymers and centers
groupABCD = group.union(name="abcd-particles", a=groupAB, b=groupCD) #all

###############Rigid Body
rigid = md.constrain.rigid();
#rigid.set_param('A',types=['B']*59,positions=[(0.5563,0,2.4373),(-0.27815,0.48177,2.4373),(-0.27815,-0.48177,2.4373),(1.5587,0,1.9546),(1.1022,1.1022,1.9546),(9.5444e-17,1.5587,1.9546),(-1.1022,1.1022,1.9546),(-1.5587,1.9089e-16,1.9546),(-1.1022,-1.1022,1.9546),(-2.8633e-16,-1.5587,1.9546),(1.1022,-1.1022,1.9546),(2.2524,0,1.0847),(1.9507,1.1262,1.0847),(1.1262,1.9507,1.0847),(1.3792e-16,2.2524,1.0847),(-1.1262,1.9507,1.0847),(-1.9507,1.1262,1.0847),(-2.2524,2.7584e-16,1.0847),(-1.9507,-1.1262,1.0847),(-1.1262,-1.9507,1.0847),(-4.1376e-16,-2.2524,1.0847),(1.1262,-1.9507,1.0847),(1.9507,-1.1262,1.0847),(2.5,0,1.5308e-16),(2.2136,1.1618,1.5308e-16),(1.4202,2.0575,1.5308e-16),(0.30134,2.4818,1.5308e-16),(-0.88651,2.3375,1.5308e-16),(-1.8713,1.6578,1.5308e-16),(-2.4274,0.59829,1.5308e-16),(-2.4274,-0.59829,1.5308e-16),(-1.8713,-1.6578,1.5308e-16),(-0.88651,-2.3375,1.5308e-16),(0.30134,-2.4818,1.5308e-16),(1.4202,-2.0575,1.5308e-16),(2.2136,-1.1618,1.5308e-16),(2.2524,0,-1.0847),(1.9507,1.1262,-1.0847),(1.1262,1.9507,-1.0847),(1.3792e-16,2.2524,-1.0847),(-1.1262,1.9507,-1.0847),(-1.9507,1.1262,-1.0847),(-2.2524,2.7584e-16,-1.0847),(-1.9507,-1.1262,-1.0847),(-1.1262,-1.9507,-1.0847),(-4.1376e-16,-2.2524,-1.0847),(1.1262,-1.9507,-1.0847),(1.9507,-1.1262,-1.0847),(1.5587,0,-1.9546),(1.1022,1.1022,-1.9546),(9.5444e-17,1.5587,-1.9546),(-1.1022,1.1022,-1.9546),(-1.5587,1.9089e-16,-1.9546),(-1.1022,-1.1022,-1.9546),(-2.8633e-16,-1.5587,-1.9546),(1.1022,-1.1022,-1.9546),(0.5563,0,-2.4373),(-0.27815,0.48177,-2.4373),(-0.27815,-0.48177,-2.4373)]);
#rigid.set_param('A',types=['B']*3,positions=[(2.5,0,0),(-1.25,2.1651,0),(-1.2500,-2.1651 ,0)])
rigid.set_param('A',types=['B']*16,positions=[ (0.38268,0,0.92388),(-0.38268,4.6865e-17,0.92388),(0.92388,0,0.38268),(0.46194,0.8001,0.38268),(-0.46194,0.8001,0.38268),(-0.92388,1.1314e-16,0.38268),(-0.46194,-0.8001,0.38268),(0.46194,-0.8001,0.38268),(0.92388,0,-0.38268),(0.46194,0.8001,-0.38268),(-0.46194,0.8001,-0.38268),(-0.92388,1.1314e-16,-0.38268),(-0.46194,-0.8001,-0.38268),(0.46194,-0.8001,-0.38268),(0.38268,0,-0.92388),(-0.38268,4.6865e-17,-0.92388)])
rigid.validate_bodies()


#Ext=md.external.periodic()
#Ext.force_coeff.set('A', A=1.0, i=0, w=0.02, p=3)
#Ext.force_coeff.set(['B','C','D'], A=-0.1, i=0, w=0.02, p=3)




md.integrate.mode_standard(dt=0.02)
nve=md.integrate.nve(group=groupACD)


#nl.tune(warmup=10000, r_min=0.05, r_max=4.0, jumps=20, steps=100, set_max_check_period=False, quiet=False)
nl.set_params(r_buff=1.5)

if restart_flag==0:
	run(10000) #mixup at beginning
	xmlmix = deprecated.dump.xml(group=group.all(), filename=prefix+"_start.xml", all=True)
	DCD1=dump.dcd(filename=prefix+"_traj.dcd", group=groupABCD, period=1000, phase=0)
	gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
	analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_harmonic_energy','pressure' ],period=100, header_prefix='#', phase=0)
	zeroer= md.update.zero_momentum(period=1)
	PCND = md.angle.harmonic()
	PCND.angle_coeff.set('Nanoparticle', k = Xi, t0 =Tau)
	alphaAB=100
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
	dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 3)
	dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)
	#####################################################
	run_upto(totaltimesteps+10000)
elif restart_flag==1:
	DCD1=dump.dcd(filename=prefix+"_traj.dcd", group=groupABCD, period=1000, phase=0)
	gsd_restart = dump.gsd(filename=prefix+'_restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
	analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_harmonic_energy','pressure' ],period=100, header_prefix='#', phase=0)
	zeroer= md.update.zero_momentum(period=1)
	PCND = md.angle.harmonic()
	PCND.angle_coeff.set('Nanoparticle', k = Xi, t0 =Tau)
	alphaAB=100
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
	dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 3)
	dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)
	#####################################################
	run_upto(totaltimesteps+10000)
	xmlfinal = deprecated.dump.xml(group=group.all(), filename=prefix+"_PCND1final.xml", all=True)
elif restart_flag==2:
	DCD1=dump.dcd(filename=prefix+"_traj.dcd", group=groupABCD, period=1000, phase=0)
	gsd_restart = dump.gsd(filename=prefix+'_AfterPCND1restart.gsd', group=group.all(), truncate=True, period=1000, phase=0)
	analyze.log(filename=prefix+'.log', quantities=['time','temperature', 'potential_energy','kinetic_energy','translational_kinetic_energy','rotational_kinetic_energy','pair_dpd_energy', 'bond_harmonic_energy','pair_slj_energy','angle_harmonic_energy','pressure' ],period=100, header_prefix='#', phase=0)
	zeroer= md.update.zero_momentum(period=1)
	alphaAB=100
	###################################################
	dpd.pair_coeff.set('A', 'A', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('A', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'B', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'C', A=0, gamma = 3)
	dpd.pair_coeff.set('B', 'D', A=0, gamma = 3)
	dpd.pair_coeff.set('C', 'C', A=25, gamma = 3)
	dpd.pair_coeff.set('C', 'D', A=alphaAB, gamma = 3)
	dpd.pair_coeff.set('D', 'D', A=25, gamma = 3)
	#####################################################
	run_upto(totaltimesteps+10000+timesteps_afterPCND)
	xmlfinal_after_PCND = deprecated.dump.xml(group=group.all(), filename=prefix+"_AfterPCND1final.xml", all=True)

