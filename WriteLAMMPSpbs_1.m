for aggsize=[288];
    S=20%for all
    %HoldatdifferentT
    %MixFirst
    %varyT
    for T=[150 175 200 225 250 275 300 350 400 500] %millions of timesteps
        filename=['AS' num2str(aggsize) 'S' num2str(S) 'T' num2str(T) 'MixFirst.pbs'];
        fid=fopen(filename,'w');
        fprintf(fid,'#!/bin/bash\n#PBS -q checkpt\n#PBS -l nodes=1:ppn=48\n#PBS -l walltime=72:00:00\n#PBS -j oe\n')
        fprintf(fid,['#PBS -N AS' num2str(aggsize) 'S' num2str(S) 'T' num2str(T) 'MixFirst' ])
        fprintf(fid,'\n#PBS -A loni_asph2023\n')
        fprintf(fid,'\n\nmodule purge\nmodule load lammps/20200303/intel-19.0.5-mvapich-2.3.3\n\n')
        fprintf(fid,['cd /work/apeters/240227/AggThroughT/HoldatdifferentT/MixFirst/T' num2str(T) '/' ])
        fprintf(fid,'\n\n\nsrun -n48 lmp -in test.in\n\n')
    end
end
