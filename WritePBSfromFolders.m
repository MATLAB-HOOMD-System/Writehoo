PATH='/mnt/Shared_Data/PlasticRoads/CoarseGraining/TestDissolusion/Run6/SARA/'
% QBPATH='/work/apeters/240828/'
template='run.template'

LineForJobName=6;
LineForCDToPath=12;

fclose all


%for all folders
fnames=dir(PATH)

for j=3:length(fnames)
    if fnames(j).isdir %&& startsWith(fnames(j).name,'PET')

        writeID=fopen([PATH fnames(j).name '.pbs'],'w')
        x=readlines([PATH template]);
        for i=1:LineForJobName-1
            fprintf(writeID,[char(x(i)) '\n']);
        end
        fprintf(writeID,['#PBS -N ' fnames(j).name '\n']);
        for i=LineForJobName+1:LineForCDToPath-1
            fprintf(writeID,[char(x(i)) '\n']);
        end
        fprintf(writeID,[char(x(LineForCDToPath)) fnames(j).name '\n']);
        for i=LineForCDToPath+1:size(x,1)
            fprintf(writeID,[char(x(i)) '\n']);
        end
        fclose all
    end
end
