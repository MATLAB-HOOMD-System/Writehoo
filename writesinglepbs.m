function writesinglepbs(varargin)
%writesinglepbs('namepath','/path/to/py/files','LONIpath','/path/to/LONI/files/')
p=inputParser;

addParameter(p,'wallclocktime',72) %hours
addParameter(p,'buffertime',5) %minutes
addParameter(p,'name',[]) 
addParameter(p,'namepath',[]) 
addParameter(p,'jobname','empty')
addParameter(p,'LONIpath','/work/apeters/')
addParameter(p,'numGPUs',1)
addParameter(p,'nodes',1)
addParameter(p,'CPUorGPU','GPU')
addParameter(p,'allocation','empty')
addParameter(p,'email','apeters@email.latech.edu')
addParameter(p,'queue','workq')
addParameter(p,'clustername','QBC')
addParameter(p,'ExtraInstructions',[])

parse(p,varargin{:});
P=p.Results
fields=fieldnames(p.Results);
for i=1:numel(fields)
    eval([fields{i} '=' 'p.Results.' fields{i} ';']);
end

if isempty(namepath)
    writingpbs(P)
else
    files=dir(namepath);
    if size(files,1)<3
        error('no files in folder')
    end
    files=files(3:end);
    NumberOfFiles=0;
    for j=1:size(files,1)
        if endsWith(files(j).name,'.py')
            NumberOfFiles=NumberOfFiles+1;
            P.name=[files(j).name(1:end-3)];
            writingpbs(P)
        end
    end
end
if exist('NumberOfFiles','var')
    NumberOfFiles
end
end

function writingpbs(P)
    fields=fieldnames(P);
    for k=1:numel(fields)
        eval([fields{k} '=' 'P.' fields{k} ';']);
    end
    

    if isempty(name)
        error('no name specified')
    end


    if strcmp(jobname,'empty')
        jobname=name;
    end


    fid=fopen([name '.pbs'],'w','l')

    if strcmp(clustername,'QBC')
        fprintf(fid,'#!/bin/bash\n');
        fprintf(fid,['#SBATCH -N ' num2str(nodes) '\n']);
        fprintf(fid,['#SBATCH -n 48\n']);
        fprintf(fid,['#SBATCH -t ' num2str(wallclocktime) ':00:00\n']);
        fprintf(fid,['#SBATCH -p ' queue '\n']);
        fprintf(fid,['#SBATCH --output="' jobname '_end.o"\n']);
    elseif strcmp(clustername,'QB2')
        fprintf(fid,['#PBS -l walltime=' num2str(wallclocktime) ':00:00,nodes=' num2str(nodes) ':ppn=20\n']);
        fprintf(fid,['#PBS -N ' jobname '\n']);
        fprintf(fid,['#PBS -o ' jobname '_end.o' '\n']);
        fprintf(fid,['#PBS -e ' jobname '_end.e' '\n']);
        fprintf(fid,['#PBS -q ' queue ' \n']);
        if ~strcmp(allocation,'empty')
            fprintf(fid,['#PBS -A ' allocation ' \n']);
        end
        fprintf(fid,'#PBS -m abe\n');
        fprintf(fid,['#PBS -M ' email '\n']);
    else
        error('Clustername specified incorrectly. Must equal QBC or QB2')
    end
    fprintf(fid,'\n');
    fprintf(fid,['\n']);

    % strpath=['cd ' LONIpath ' && '];

    fprintf(fid,['cd ' LONIpath '\n']);
%     fprintf(fid,'module load cuda/10.2.89 \n');
    fprintf(fid,[ExtraInstructions ' \n']);
    fprintf(fid,['export HOOMD_WALLTIME_STOP=$((`date +%%s` +  (' num2str(wallclocktime) '*60*60)-(' num2str(buffertime) '*60)))' '\n']);

    % strpath=['cd ' LONIpath ' && '];


    if strcmp(CPUorGPU,'GPU')
        if numGPUs==1 
            if nodes~=1
                warning('Using 1 GPU, forcing single node');
            end
            str=['python ' name '.py --mode=gpu --msg-file=''' name '.o'''];
        else
            warning('using 2 gpus and mpi');
            str=['mpirun -n 2 python ' name '.py --mode=gpu --msg-file=''' name '.o''']
        end
    elseif strcmp(CPUorGPU,'CPU')
        warning('using CPU');
        if nodes==1
            warning('using 1 node')
            str=['python ' name '.py --mode=cpu --msg-file=''' name '.o'''];
        else
            warning(['using ' num2str(nodes) ' nodes'])
            str=['mpirun -n ' numstr(nodes) 'python ' name '.py --mode=cpu --msg-file=''' name '.o'''];
        end
    end

    % str=[strpath str '\n'];
    str=[str '\n'];
    fprintf(fid,str)
    fprintf(fid,['\n'])
end



