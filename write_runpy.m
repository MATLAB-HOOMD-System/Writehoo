Nr = [1.5*ones(1,5) 3*ones(1,4) 0.5 3];
Xs = [0.05 0.075 0.025 0.01 0 0.05 0.1 0.05 0.1 0 0];
Taus = [100000 100000 100000 100000 0 1000000 1000000 100000 100000 0 0];



filename1='runtitan4.sh';
filename2='run1080ti4.sh';

fid1=fopen(filename1,'w','l')
fid2=fopen(filename2,'w','l')

for i=1:length(Nr)
    if mod(i,2)==1
        if Nr(i)==1.5
            path='/mnt/RAID1/Insync/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr1p5_numgrafts10_lengthgrafts20_fracA_0p5/'
        elseif Nr(i)==3
            path='/mnt/RAID1/Insync/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr3_numgrafts40_lengthgrafts20_fracA_0p5/'
        elseif Nr(i) == 0.5
            path='/mnt/RAID1/Insync/Shared_Data/Nanoparticles/Nr0p5_numgrafts2_lengthgrafts20_fracA_0p5/'
        end
        fprintf(fid1,['cd ' path '\n']);
        
        if Xs(i)==0
            commandname=['python PCND_Nano_NoPCND_long.py --mode=gpu --msg-file=''PCND_Nano_NoPCND_long.o''\n']
        else
            commandname=['python PCND_Nano_Xi' num2str(Xs(i)) '_t' num2str(Taus(i)) '_long.py --mode=gpu --msg-file=''PCND_Nano_Xi' num2str(Xs(i)) '_t' num2str(Taus(i)) '_long.o''\n'];
        end
        
        fprintf(fid1,commandname);
    else
        if Nr(i)==1.5
            path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr1p5_numgrafts10_lengthgrafts20_fracA_0p5/'
        elseif Nr(i)==3
            path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr3_numgrafts40_lengthgrafts20_fracA_0p5/'
        elseif Nr(i) == 0.5
            path='/mnt/Shared_Data/Nanoparticles/PCND_Nano_SLJDPD/Nr0p5_numgrafts2_lengthgrafts20_fracA_0p5/'
        end
        fprintf(fid2,['cd ' path '\n']);
        
        if Xs(i)==0
            commandname=['python PCND_Nano_NoPCND_long.py --mode=gpu --msg-file=''PCND_Nano_NoPCND_long.o''\n']
        else
            commandname=['python PCND_Nano_Xi' num2str(Xs(i)) '_t' num2str(Taus(i)) '_long.py --mode=gpu --msg-file=''PCND_Nano_Xi' num2str(Xs(i)) '_t' num2str(Taus(i)) '_long.o''\n'];
        end
        
        fprintf(fid2,commandname);
    end
end
        