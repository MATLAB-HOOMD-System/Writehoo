addpath('/media/andrew/External_Data/Google Drive LATECH/micelle_dynamics/ABA_vary_conc_0.001_0.02_12-18-17')

clear all
fclose all
clc
volfracs=[0.001:0.001:0.01 0.025 0.05 0.1]
size_fracs=[80 80 80 ones(1,10)*65]
   
for j=1:length(volfracs)
    volfrac=volfracs(j)
    size_frac=size_fracs(j)
    filename=['ABA_varyconc_12-18_size' num2str(size_frac) '_frac' num2str(volfrac)]
    fid1=fopen('hootemplate_12-18-17','r');
    fid2=fopen([filename '.py'],'w','l');
 

    for i=1:13
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
    end
    
      x=fgetl(fid1);
      fprintf(fid2,['frac=' num2str(volfrac) '\n']);
      
      x=fgetl(fid1);
      fprintf(fid2,['boxdim=' num2str(size_frac) '\n']);
          
        for i=16:93
            x=fgetl(fid1);
            fprintf(fid2,x);
            fprintf(fid2,'\n');
        end
        
end
fclose all
          