function write_multinode_GPU(varargin)

%write_multinode_GPU('queue','workq','wallclocktime',37)
p=inputParser;

path='~/MATLAB/pbs/'
files=dir(path);
if length(files)<4;
    defaultpbs1='placeholder';
    defaultpbs2=defaultpbs1
else
    defaultpbs1=files(3).name;
    defaultpbs2=files(4).name;
end

addParameter(p,'pbs1',defaultpbs1)
addParameter(p,'pbs2',defaultpbs2)
addParameter(p,'pbsname','default')
addParameter(p,'queue','empty')
addParameter(p,'wallclocktime',0)
addParameter(p,'allocation','empty')
addParameter(p,'firststring','empty')


parse(p,varargin{:});
fields=fieldnames(p.Results);
for i=1:numel(fields)
    eval([fields{i} '=' 'p.Results.' fields{i} ';']);
end



if exist('pbs1','var')~=2
    path='~/MATLAB/pbs/'
    files=dir(path);
%     filenames=makefilenames(files)
    if length(files)<4
        error('files not specifed properly');
    else 
        if mod(length(files),2)~=0
            error('odd number of files');
        end
        
        %% sort files
        if ~strcmp(firststring,'empty')
            for i=4:length(files)
                if ~isempty(findstr(files(i).name,firststring)) %if it finds the string
                    tempstr=files(i).name;
                    for j=3:i-1
                        tempstrs{j}=files(j).name;
                    end
                    for j=4:i
                        files(j).name=tempstrs{j-1};
                    end
                    files(3).name=tempstr;
                end
            end
        end
%         filenames2=makefilenames(files)
        %%
        for i=3:2:length(files)
            pbs1=[path files(i).name];
            pbs2=[path files(i+1).name];
            writeboth(pbs1,pbs2,pbsname,queue,wallclocktime,allocation)
        end
    end

end

end
    
function filenames=makefilenames(files)
    for i=1:length(files)
        filenames{i}=files(i).name;
    end
end

function writeboth(pbs1,pbs2,pbsname,queue,wallclocktime,allocation)
fid1=fopen(pbs1,'r');
fid2=fopen(pbs2,'r');

testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'walltime='));
        testpython=1;
    end
end
xnum=strfind(x,'walltime=');
walltime1=str2num(x(xnum+9:xnum+9+1));

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'walltime='));
        testpython=1;
    end
end
xnum=strfind(x,'walltime=');
walltime2=str2num(x(xnum+9:xnum+9+1));
walltime=max([walltime1 walltime2]);

testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,' -q '));
        testpython=1;
    end
end
xnum=strfind(x,' -q ');
if strcmp(queue,'empty');
    queue= x(9:end);
end


testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'cd '));
        testpython=1;
    end
end
path1=x(4:end);

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'cd '));
        testpython=1;
    end
end
path2=x(4:end);


testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'export HOOMD_WALLTIME_STOP'));
        testpython=1;
    end
end
walltimestopline=x;



testpython=0;
while testpython==0
    x=fgetl(fid1);
    if ~isempty(strfind(x,'python '));
        testpython=1;
    end
end
runstring1=x;
pystring1=x(8:strfind(x,'.py ')+3);

testpython=0;
while testpython==0
    x=fgetl(fid2);
    if ~isempty(strfind(x,'python '));
        testpython=1;
    end
end
runstring2=x;
pystring2=x(8:strfind(x,'.py ')+3);

%check pbswritefile, update, and change pbsname if necessary
if exist('doublepbswritefile.mat','file')~=2
    day=datetime('today');
    pbscounter=1;
    save('doublepbswritefile.mat','day','pbscounter')
end
load('doublepbswritefile.mat')
if datetime('today')~=day;
    day=datetime('today');
    pbscounter=1;
end
if pbsname=='default' 
    pbsname=[datestr(day) '-' num2str(pbscounter)];
end
pbscounter=pbscounter+1;
save('doublepbswritefile.mat','day','pbscounter')
if wallclocktime~=0
    walltime=wallclocktime;
end
    

fid=fopen([pbsname '.pbs'],'w','l')


fprintf(fid,'#!/bin/bash\n');
fprintf(fid,['#PBS -l walltime=' num2str(walltime) ':00:00,nodes=1:ppn=20\n']);
fprintf(fid,['#PBS -N ' pbsname '\n']);
fprintf(fid,['#PBS -o ' pbsname '_end.o' '\n']);
fprintf(fid,['#PBS -e ' pbsname '_end.e' '\n']);
fprintf(fid,['#PBS -q ' queue ' \n']);
if ~strcmp(allocation,'empty')
    fprintf(fid,['#PBS -A ' allocation ' \n']);
end
fprintf(fid,'#PBS -m abe\n');
fprintf(fid,'#PBS -M apeters@latech.edu\n');
fprintf(fid,'\n');
fprintf(fid,['\n']);
fprintf(fid,'module load cuda/9.0.176 \n');
%         fprintf(fid,'export SOFTWARE_ROOT=~/hoomd-install\n');
%         fprintf(fid,'export PYTHONPATH=$PYTHONPATH:${SOFTWARE_ROOT}/lib/python\n');
fprintf(fid,['export HOOMD_WALLTIME_STOP=$((`date +%%s` +  (' num2str(walltime) '*60*60)-(3*60)))' '\n']);


fprintf(fid,['cat $PBS_NODEFILE > nodefile1\n\nexec 5< nodefile1\nfilecounter=0\nwhile read line1 <&5 ; do\n        read line2 <&5\n        read line3 <&5\n        read line4 <&5\n        read line5 <&5\n        read line6 <&5\n        read line7 <&5\n        read line8 <&5\n        read line9 <&5\n        read line10 <&5\n        filecounter=$(expr $filecounter + 1)\n        exec 1<>hostlist$filecounter\n        printf "$line1\\n$line2\\n$line3\\n$line4\\n$line5\\n$line6\\n$line7\\n$line8\\n$line9\\n$line10\\n" >&1\ndone\nexec 5<&-\nexec 1<&-'])



str=['(cd ' path1 ' && mpirun -n 1 python ' pystring1 ' --gpu=0 --msg-file=''' pystring1(1:end-3) 'o'') & (cd ' path2 ' && mpirun -n 1 python ' pystring2 ' --gpu=1 --msg-file=''' pystring2(1:end-3) 'o''\n\n'];

% str=['cd ' path1 ' && python ' pystring1 ' --gpu=0 --msg-file=''' pystring1(1:end-3) 'o'' & cd ' path2 ' && python ' pystring2 ' --gpu=1 --msg-file=''' pystring2(1:end-3) 'o''\n\n'];
fprintf(fid,str)
fprintf(fid,'wait\n');
fclose all
end
